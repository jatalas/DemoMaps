﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoMaps.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MapaEstatico()
        {
            return View();
        }
        public ActionResult MapaDinamico()
        {

            return View();
        }
        public ActionResult Prueba()
        {
            return View();
        }
    }
}